# AGS Piling Guidance / General

*This is a placeholder page, copied from AGSi. Requires review and edit to make it suitable for AGS Piling.*

## Governance - Naming conventions adopted in AGSi

!!! Note
    This page was originally intended to be for developer use only during the drafting of AGSi.
    However, we have decided to leave it in the documentation for now as it contains some background information that some may find useful or interesting.


**Object names** use PascalCase except that all are preceded by the prefix **ags** or **agsi** in lower case. Example: **agsiObjectName**

The object name prefix **agsi** is used for objects that are unique to the AGSi schema whereas **ags** is used for objects that may eventually be shared with other AGS schema that may be developed in the future.

!!! Note
    OGC GML standards use PascalCase but with a namespace prefix in lower case (separated by a colon), e.g. gsmlb:GeologicUnit. IFC has names starting with 'Ifc', e.g. IfcPile.*

**Attribute names** use camelCase. Example: **attributeName**

!!! Note
    OGC GML standards use camelCase. IFC uses PascalCase.

Object and attribute names should be meaningful. Full words should generally be used, except that reasonable truncation or abbreviation may be used to avoid very long names.  Example: **valueProfileIndVarCodeID**.

!!! Note
    This seems to be common convention. Applied in OGC GML standards and IFC.

Object names shall incorporate the object name of the group (which is sometimes also the top level parent) but should not include any further parent names.

Example: <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> (Model group and parent); <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> (child)


!!! Note
    Including all parent names would lead to very long and cumbersome object names.

Where one object is embedded (nested) within another (as an attribute), the parent object attribute name shall generally be the child object name. Example: <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel">agsiModel</a> has an attribute *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModel#agsimodelelement">agsiModelElement</a>*, which will contain <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Model_agsiModelElement">agsiModelElement</a> objects.

!!! Note
    Adopting this convention ensures that the object name will appear in the data file (for nested JSON encoding). However, there are cases where this is not possible, e.g. target object can be one of several different objects.

Where an attribute value is intended to be a reference to an identifier defined elsewhere in the schema, the source and target identifiers should be the same, and should end with 'ID'. Example: *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#codeid">codeID</a>* attribute in <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a> references *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode#codeid">codeID</a>* attribute in <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode">agsProjectCode</a> object.

An exception to the above is where clarification of the usage of the identifier is desirable, or required to avoid conflict. In such cases clarifying terms may be added to the source attribute name in front of the
target attribute name.

Example: *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#valueprofileindvarcodeid">valueProfileIndVarCodeID</a>* attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a>
references *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode#codeid">codeID</a>* attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Project_agsProjectCode">agsProjectCode</a> which clarifies and avoids
clash with *<a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue#codeid">codeID</a>* attribute of <a href="https://ags-data-format-wg.gitlab.io/agsi_standard/1.0.0/Standard_Data_agsiDataParameterValue">agsiDataParameterValue</a>.


### Version history

First formal issue, for v1.0.0 of standard, 30/11/2022.
